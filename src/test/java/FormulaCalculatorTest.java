import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.Random;


public class FormulaCalculatorTest {
    private Random random = new Random();
    private Double infinity = Double.POSITIVE_INFINITY;
    private int[] randomArrayDistance = {random.nextInt(),0};
    private InputValues testInputValues = new InputValues(LabelEnum.distance,randomArrayDistance);
    private FormulaCalculator calculatorTester = new FormulaCalculator() {
        @Override
        public double calculate(InputValues inputValues) {
            return super.calculate(inputValues);
        }
    };
    @Test
    public void testDivisionByZero() {
        int[] randomArrayFrequency = {random.nextInt(),random.nextInt(),random.nextInt(),0};
        int[] randomArraySpeed = {random.nextInt(),random.nextInt(),random.nextInt(),random.nextInt(),0};
        assertEquals(infinity,calculatorTester.calculate(testInputValues),0.0001);
        testInputValues.setLabelType(LabelEnum.frequency);
        testInputValues.setValueArray(randomArrayFrequency);
        assertEquals(infinity,calculatorTester.calculate(testInputValues),0.0001);
        testInputValues.setLabelType(LabelEnum.speed);
        testInputValues.setValueArray(randomArraySpeed);
        assertEquals(infinity,calculatorTester.calculate(testInputValues),0.0001);
    }

    @Test
    public void testResultAccuracy() {
        int[] distanceTestArray = {random.nextInt(),random.nextInt()};
        int[] frequencyTestArray = {random.nextInt(),random.nextInt(),random.nextInt(),random.nextInt()};
        int[] speedTestArray = {random.nextInt(),random.nextInt(),random.nextInt(),random.nextInt(),random.nextInt()};
        testInputValues.setValueArray(distanceTestArray);
        testInputValues.setLabelType(LabelEnum.distance);
        assertEquals((double)distanceTestArray[0]/distanceTestArray[1], calculatorTester.calculate(testInputValues), 0.0001);
        testInputValues.setLabelType(LabelEnum.flexibility);
        assertEquals(((double)distanceTestArray[0]/2) *distanceTestArray[1], calculatorTester.calculate(testInputValues), 0.0001);
        testInputValues.setValueArray(frequencyTestArray);
        testInputValues.setLabelType(LabelEnum.frequency);
        for(int i : frequencyTestArray) {
            System.out.println(i);
        }
        assertEquals((frequencyTestArray[0]+frequencyTestArray[1]+frequencyTestArray[2])/(double)frequencyTestArray[3],calculatorTester.calculate(testInputValues));
        testInputValues.setLabelType(LabelEnum.speed);
        testInputValues.setValueArray(speedTestArray);
        assertEquals(((speedTestArray[0] + speedTestArray[1]) * (speedTestArray[2] + speedTestArray[3])) /(double) speedTestArray[4],calculatorTester.calculate(testInputValues),0.0001);
    }
}
