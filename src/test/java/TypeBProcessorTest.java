import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

public class TypeBProcessorTest {
    private List<InputValues> testListOfValues = new ArrayList<>();
    @Test
    public void testProcessValues() {
        TypeBProcessor testTypeBProcessor = new TypeBProcessor();
        InputValues testValueOne = new InputValues(LabelEnum.bias, new int[]{1,2,3,4,5,6});
        testListOfValues.add(testValueOne);
        for(int i=0;i<testListOfValues.size();i++){
            String result = testTypeBProcessor.processValues(testListOfValues.get(i),i<testListOfValues.size()-1);
            assertEquals(LabelEnum.bias + " " + 2.0 + System.lineSeparator() + LabelEnum.bias + " " + 2.0 + System.lineSeparator(),result);
        }
        InputValues testValueTwo = new InputValues(LabelEnum.bias, new int[]{3,4,5,6,0,7});
        testListOfValues.add(testValueTwo);

        String actualResult= "";
        for(int i=0;i<testListOfValues.size();i++) {
            actualResult=actualResult.concat((testTypeBProcessor.processValues(testListOfValues.get(i),i<testListOfValues.size()-1)));
        }
        assertEquals(LabelEnum.bias + " " + 2.0 + System.lineSeparator() + LabelEnum.bias + " " + 18.0 + System.lineSeparator(),actualResult);

        InputValues testValueThree = new InputValues(LabelEnum.distance, new int[]{2,0});
        testListOfValues.add(testValueThree);
        String actualResultTwo= "";
        for(int i=0;i<testListOfValues.size();i++) {
            actualResultTwo=actualResultTwo.concat((testTypeBProcessor.processValues(testListOfValues.get(i),i<testListOfValues.size()-1)));
        }
        assertEquals(LabelEnum.bias + " " + 2.0 + System.lineSeparator() + LabelEnum.bias + " " + 18.0 + System.lineSeparator()+
                LabelEnum.distance + " " + Double.POSITIVE_INFINITY + System.lineSeparator() + LabelEnum.distance + " "+ Double.POSITIVE_INFINITY + System.lineSeparator(),actualResultTwo);
    }
}
