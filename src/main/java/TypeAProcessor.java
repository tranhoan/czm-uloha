
public class TypeAProcessor extends FormulaCalculator implements IValueProcessor {
    //processes values for file type A and returns data which are then written into the output file
    public String processValues(InputValues inputValues, boolean hasNextLine) {
        LabelEnum labelType = inputValues.getLabelType();
        double calculatedResult = this.calculate(inputValues);
        return labelType+" " +Double.toString(calculatedResult)+System.lineSeparator();
    }
}
