import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        String path = "./".concat(args[0]+".txt");
        try {
            File inputFile = new File(path);
            Scanner scanner = new Scanner(inputFile);
            String firstLine = scanner.nextLine();
            TypeEnum fileType = getFileType(firstLine);
            Path outputFile = Paths.get(args[1].concat(".txt"));
            IValueProcessor processor =  valueProcessorCreate(fileType);
            String line;
            //reading line by line to the end of file
            while(scanner.hasNextLine()) {
                line = scanner.nextLine();
                //only processes the data if the line is not empty
                if(!line.isEmpty()) {
                    //parsing the input values and getting the label
                    String[] arrayOfValues = line.split(" ");
                    LabelEnum label = LabelEnum.valueOf(arrayOfValues[0]);
                    String[] arrayOfStringValues = Arrays.copyOfRange(arrayOfValues,1,arrayOfValues.length);
                    int[] finalArrayOfValues = Arrays.stream(arrayOfStringValues).mapToInt(Integer::parseInt).toArray();
                    InputValues inputValues = new InputValues(label,finalArrayOfValues);
                    String outputValue = processor.processValues(inputValues,scanner.hasNextLine());
                    System.out.println(outputValue);
                    //writing data into the file
                    Files.write(outputFile,outputValue.getBytes(),StandardOpenOption.CREATE, StandardOpenOption.APPEND);
                }
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            System.out.println("File could not be found.");
        } catch (IOException e) {
            System.out.println("Could not read line");
        }
    }
    //instantiates processor depending on the label in the input file
    public static IValueProcessor valueProcessorCreate(TypeEnum typeEnum) {
        switch (typeEnum){
            case A: return new TypeAProcessor();
            case B: return new TypeBProcessor();
            default: return null;
        }
    }
    public static TypeEnum getFileType(String stringFileType) {
        switch (stringFileType){
            case "A": return TypeEnum.A;
            case "B": return TypeEnum.B;
            default: return null;
        }
    }
}
