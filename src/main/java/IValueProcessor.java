
public interface IValueProcessor {
    String processValues(InputValues inputValues, boolean hasNextLine);
}
