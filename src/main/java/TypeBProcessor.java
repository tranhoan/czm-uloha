
public class TypeBProcessor extends FormulaCalculator implements IValueProcessor {
    private double minimum;
    private double maximum;
    private LabelEnum previousLabel;
    private boolean isFirstIteration = true;

    @Override
    //processes input values in one line and returns a data which are then written into the output file
    public String processValues(InputValues inputValues,boolean hasNextLine) {
        double calculatedResult = this.calculate(inputValues);
        //detects the first line of input values with a label
        if (isFirstIteration) {
            previousLabel = inputValues.getLabelType();
            minimum = calculatedResult;
            maximum = calculatedResult;
            isFirstIteration = false;
            //returns empty string if the file has a next line otherwise returns minimum and maximum
            return hasNextLine ? "" : previousLabel+" "+Double.toString(minimum)+System.lineSeparator() + previousLabel+" "+Double.toString(maximum)+System.lineSeparator();
        }
        //if the next line contains the same label as the previous one recalculate the max and min
        if (previousLabel == inputValues.getLabelType()) {
            if (calculatedResult < minimum) {
                minimum = calculatedResult;
            } else if (calculatedResult > maximum) {
                maximum = calculatedResult;
            }
            return hasNextLine ? "" : previousLabel+" " +Double.toString(minimum)+System.lineSeparator() + previousLabel+" "+Double.toString(maximum)+System.lineSeparator();

        } else {
            //if the new line contains a different label than the previous one
            double oldMinimum = minimum;
            double oldMaximum = maximum;
            LabelEnum oldLabel = previousLabel;
            minimum = calculatedResult;
            maximum = calculatedResult;
            previousLabel = inputValues.getLabelType();
            String dataToWrite = (oldLabel+" " +Double.toString(oldMinimum)+System.lineSeparator() + oldLabel+" "+Double.toString(oldMaximum)+System.lineSeparator());
            //returns all of the collected data if there's nothing left to read
            return hasNextLine
                    ? dataToWrite
                    : dataToWrite + previousLabel + " " + Double.toString(minimum)+System.lineSeparator() + previousLabel + " " + Double.toString(maximum) + System.lineSeparator();
        }

    }

}
