/**
 * Created by celer on 7/14/2018.
 */
public enum LabelEnum {
    bias("bias", 6), distance("distance", 2), flexibility("flexibility", 2), frequency("frequency", 4), humidity("humidity", 3), speed("speed",5);

    private final String label;
    private final int numberOfValues;

    LabelEnum(String label, int numberOfValues) {
        this.label = label;
        this.numberOfValues = numberOfValues;
    }

    public String getLabel() {
        return label;
    }

    public int getNumberOfValues() {
        return numberOfValues;
    }
}
