
public class InputValues {
    //each line contains the same format <label>  <list of int>
    private LabelEnum labelType;
    private int[] valueArray;

    public InputValues(LabelEnum labelType, int[] valueArray) {
        this.labelType = labelType;
        this.valueArray = valueArray;
    }

    public LabelEnum getLabelType() {
        return labelType;
    }

    public int[] getValueList() {
        return valueArray;
    }

    public void setLabelType(LabelEnum labelType) {
        this.labelType = labelType;
    }

    public void setValueArray(int[] valueArray) {
        this.valueArray = valueArray;
    }
}
