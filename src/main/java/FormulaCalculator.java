
public abstract class FormulaCalculator {
    //calculates the result based on the formula for a label
    public double calculate(InputValues inputValues) {
        LabelEnum labelType = inputValues.getLabelType();
        int[] valueList = inputValues.getValueList();
        double infinity = Double.POSITIVE_INFINITY;
        double result;

        switch (labelType) {
            case bias:
                result = (double)valueList[0] * valueList[1] + valueList[2] - valueList[3] - valueList[4] + valueList[5];
                break;
            case distance:
                if (valueList[1] == 0) {
                    result = infinity;
                } else {
                    result = (double) valueList[0] / valueList[1];
                }
                break;
            case flexibility:
                result =  ((double)valueList[0] / 2) * valueList[1];
                break;
            case frequency:
                if (valueList[3] == 0) {
                    result = infinity;
                } else {
                    result = (double)(valueList[0] + valueList[1] + valueList[2]) / valueList[3];
                }
                break;
            case speed:
                if (valueList[4] == 0) {
                    result = infinity;
                } else {
                    result = (double)((valueList[0] + valueList[1]) * (valueList[2] + valueList[3])) / valueList[4];
                }
                break;
            default:
                result = (double)valueList[0] * valueList[1] * valueList[2];
                break;
        }
        return result;
    }
}
